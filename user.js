// Misc privacy: Disk
user_pref("signon.rememberSignons", false);
user_pref("browser.formfill.enable", false);
user_pref("signon.autofillForms", false);
user_pref("browser.sessionstore.privacy_level", 2);

// Misc privacy: Remote
user_pref("browser.send_pings", false);
user_pref("geo.enabled", false);
user_pref("geo.wifi.uri", "");
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.blockedURIs.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.provider.google.updateURL", "");
user_pref("browser.safebrowsing.provider.google.gethashURL", "");
user_pref("browser.safebrowsing.provider.google4.updateURL", "");
user_pref("browser.safebrowsing.provider.google4.gethashURL", "");
user_pref("browser.safebrowsing.provider.mozilla.updateURL", "");
user_pref("browser.safebrowsing.provider.mozilla.gethashURL", "");
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
// Make sure Unified Telemetry is really disabled, see: #18738.
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("experiments.enabled", false);
user_pref("identity.fxaccounts.enabled", false);
user_pref("services.sync.engine.prefs", false); 
user_pref("services.sync.engine.addons", false);
user_pref("services.sync.engine.tabs", false);
user_pref("extensions.getAddons.cache.enabled", false);
user_pref("browser.search.region", "US");
user_pref("browser.search.geoip.url", "");
user_pref("browser.fixup.alternate.enabled", false);
// Make sure there is no Tracking Protection active in Tor Browser, see: #17898.
user_pref("privacy.trackingprotection.pbmode.enabled", false);
// Disable the Pocket extension (Bug #18886 and #31602)
user_pref("extensions.pocket.enabled", false);
user_pref("network.http.referer.hideOnionSource", true);

// Disable ServiceWorkers by default
user_pref("dom.serviceWorkers.enabled", false);

// Fingerprinting
user_pref("gfx.downloadable_fonts.fallback_delay", -1);
user_pref("browser.link.open_newwindow.restriction", 0);
// Set video VP9 to 0 for everyone (bug 22548)
user_pref("media.benchmark.vp9.threshold", 0);
user_pref("dom.enable_resource_timing", false);
user_pref("privacy.resistFingerprinting", true);
user_pref("privacy.resistFingerprinting.block_mozAddonManager", true);
user_pref("dom.vr.enabled", false);
user_pref("browser.cache.frecency_experiment", -1);
// Bug 2874: Block Components.interfaces from content
user_pref("dom.use_components_shim", false);
// Disable network information API everywhere. It gets spoofed in bug 1372072
// but, alas, the behavior is inconsistent across platforms, see:
// https://trac.torproject.org/projects/tor/ticket/27268#comment:19. We should
// not leak that difference if possible.
user_pref("dom.netinfo.enabled", false);
user_pref("network.http.referer.defaultPolicy", 2);

// Third party stuff
user_pref("network.cookie.cookieBehavior", 1);

// Proxy and proxy security
user_pref("network.proxy.socks", "127.0.0.1");
user_pref("network.proxy.socks_port", 9050);
user_pref("network.proxy.socks_remote_dns", true);
user_pref("network.proxy.no_proxies_on", "");
user_pref("network.proxy.allow_hijacking_localhost", true);
user_pref("network.security.ports.banned", "9050,9051,9150,9151");
user_pref("network.dns.disablePrefetch", true);
user_pref("plugins.click_to_play", true);
// Make sure no enterprise policy can interfere with our proxy settings, see
// #29916.
user_pref("browser.policies.testing.disallowEnterprise", true);

// Network and performance
user_pref("security.ssl.enable_false_start", true);
user_pref("network.http.connection-retry-timeout", 0);
user_pref("network.http.max-persistent-connections-per-proxy", 256);
user_pref("network.manage-offline-status", false);
// No need to leak things to Mozilla, see bug 21790
user_pref("network.captive-portal-service.enabled", false);
// As a "defense in depth" measure, configure an empty push server URL (the
// DOM Push features are disabled by default via other prefs).
user_pref("dom.push.serverURL", "");

// Bug 28896: Make sure our bundled WebExtensions are running in Private Browsing Mode
user_pref("extensions.allowPrivateBrowsingByDefault", true);

// Make sure we use the same search engine regardless of locale
user_pref("browser.search.geoSpecificDefaults", false);

// Enforce certificate pinning, see: https://bugs.torproject.org/16206
user_pref("security.cert_pinning.enforcement_level", 2);

// Don't allow MitM via Microsoft Family Safety, see bug 21686
user_pref("security.family_safety.mode", 0);

// Don't allow MitM via enterprise roots, see bug 30681
user_pref("security.enterprise_roots.enabled", false);

// Don't ping Mozilla for MitM detection, see bug 32321
user_pref("security.certerrors.mitm.priming.enabled", false);

// Avoid report TLS errors to Mozilla. We might want to repurpose this feature
// one day to help detecting bad relays (which is bug 19119). For now we just
// hide the checkbox, see bug 22072.
user_pref("security.ssl.errorReporting.enabled", false);

// Treat .onions as secure
user_pref("dom.securecontext.whitelist_onions", true);

// Disable Presentation API
user_pref("dom.presentation.controller.enabled", false);
user_pref("dom.presentation.enabled", false);
user_pref("dom.presentation.discoverable", false);
user_pref("dom.presentation.discoverable.encrypted", false);
user_pref("dom.presentation.discovery.enabled", false);
user_pref("dom.presentation.receiver.enabled", false);

user_pref("dom.audiochannel.audioCompeting", false);
user_pref("dom.audiochannel.mediaControl", false);

// Bug 31144 - Avoid proxy-bypass using the Android native download manager.
user_pref("browser.download.forward_oma_android_download_manager", false);
