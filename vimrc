set history=10000 " more history
set shortmess+=I " disable startup message
set autochdir " set current directory to directory of last opened file
filetype plugin on " load file-type specific plugin files
filetype indent on " load file-type specific indent files
set hidden " allow hidden buffers (not displayed in any window)
set mouse+=a " enable mouse support
set cursorline " highlight current line
set backspace=indent,eol,start " allow backspacing over everything
syntax on " turn on syntax highlighting

set scrolloff=20 " show 5 lines above and below cursor (when possible)
set listchars=tab:>>,nbsp:~ " set list to see tabs and non-breakable spaces
set number " line numbering
set relativenumber " relative line numbering
" toggle relative numbering (default: <C-n> j)
nnoremap <C-n> :set relativenumber!<CR>

set autoindent " copy indent from current line to new line
set expandtab " expand tabs to spaces
set tabstop=4 " tabs are expanded to 4 spaces in the file
set shiftwidth=4 " 4 spaces for each indent
set softtabstop=4 " tabs are displayed as 4 spaces on screen
" indent visual selection with tab
vnoremap <Tab> >
vnoremap <S-Tab> <

set incsearch " incremental search (as string is being typed)
set ignorecase " ignore case for searching
set smartcase " override ignore case when upper case letters are present
set hlsearch " highlight search
" turn off search highlighting (default: <C-h> h)
nnoremap <C-h> :set nohlsearch<CR>

" readline movement shortcuts in normal mode
" cursor to start of line (default: insert previously inserted text)
inoremap <C-A> <C-Home>
" cursor to end of line (default: insert character which is below cursor)
inoremap <C-E> <C-End>

set splitright " open new split panes to right
set splitbelow " open new split panes to bottom

" save read-only files
command -nargs=0 Sudow w !sudo tee % >/dev/null

set undofile " maintain undo history between sessions
set undodir=~/.vim/undodir
